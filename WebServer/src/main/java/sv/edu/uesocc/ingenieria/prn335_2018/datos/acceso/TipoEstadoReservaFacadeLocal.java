/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoReserva;

/**
 *
 * @author kevin
 */
@Local
public interface TipoEstadoReservaFacadeLocal extends MetodosGenericos<TipoEstadoReserva>{

   public List<TipoEstadoReserva> buscarPorNombre(String nombre);
    
}
